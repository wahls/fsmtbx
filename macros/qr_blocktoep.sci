function [Q,R] = qr_blocktoep(BC,BR)
//
// Purpose:
// 
//	QR factorization of a block Toeplitz matrix.
//
// Description:
// 
//	Suppose we want to factorize a block Toeplitz matrix T with r x s
//	blocks, first block column BC and first block row BR. Then, Q and R are
//	such that Q is orthonormal, R is upper triangular and T=Q*R. We have
//	to assume that the matrix [T'*T T';T eye()] is strongly regular (i.e.,
//	all leading minors are non-zero).
//
// Algorithm:
// 
//	See D. Kressner, P. Van Dooren: "Factorizations and linear system
//	solvers for matrices with Toeplitz structure", SLICOT Working Note
//	2000-2, available: http://www.slicot.org.
//
// Authors:
//
// 	Sander WAHLS
//

  // check inputs
  if argn(2)~=2 then
    error("Expecting two inputs");
  end
  if typeof(BC)~="constant" then
    error("BC is no matrix");
  end
  if typeof(BR)~="constant" then
    error("BR is no matrix");
  end
  [m1,s] = size(BC);
  [r,n2] = size(BR);
  M = m1/r;	// number of blocks (vertical)
  if M~=round(M) then
    error("Height of BC is not a integer multiple of r");
  end
  N = n2/s;	// number of blocks (horizontal)
  if N~=round(N) then
    error("Width of BR is not a integer multiple of s");
  end

  // factorize first block
  [Q0,R0] = qr(BC,"e");

  // compute the product Q0'*(block Toeplitz matrix) and
  // the last row of the block Toeplitz matrix
  col = BC;
  S = Q0'*col;
  last_row = col($-r+1:$,:);
  for k=2:N do
    col = [BR(:,(k-1)*s+1:k*s);col(1:$-r,:)];
    S = [S Q0'*col];
    last_row = [last_row col($-r+1:$,:)];
  end

  // compute the generator for [T'*T T';T eye()]
  GR = [S;zeros(r,s) BR(:,s+1:$);zeros(s,s) S(:,s+1:$);zeros(r,s) ...
    last_row(:,1:$-s)];
  GQ = [Q0';eye(r,M*r);Q0';zeros(r,M*r)];
  G = [GR GQ]';

  // construct the shift matrix (ldl_gs requires sparse format!)
  F1 = [spzeros(s,N*s);speye((N-1)*s,(N-1)*s) spzeros((N-1)*s,s)];
  F2 = [spzeros(r,M*r);speye((M-1)*r,(M-1)*r) spzeros((M-1)*r,r)];
  F = [F1 spzeros(N*s,M*r);spzeros(M*r,N*s) F2];

  // compute LDL factorization of [T'*T T';T eye()], stop after N*s steps
  [L,d] = ldl_gs(F,G,r+s,r+s,N*s);

  // extract Q and R
  Q = L(N*s+1:$,1:N*s);
  R = L(1:N*s,1:N*s)';

endfunction