function [L,d] = ldl_blocktoep(BC)
//
// Purpose:
// 
//	LDL factorization of a strongly regular hermitian block Toeplitz matrix
//	with positive-definite first block.
//
// Description:
// 
//	Suppose we want to factorize a strongly regular (i.e., all leading
//	minors are non-zero) hermitian block Toeplitz matrix T with r x r blocks
//	and first block column BC. We assume that the first block BC(1:r,1:r) is
//	positive definite. The algorithm finds a lower triangular square matrix
//	L and a column vector d with entries +1 and -1 such that T=L*diag(d)*L'.
//
// Algorithm:
// 
//	We apply the generalized Schur algorithm, cf. the help of ldl_gs.
//
// Authors:
//
// 	Sander WAHLS
//

  // check inputs
  if argn(2)~=1 then
    error("Expecting one input.");
  end
  if typeof(BC)~="constant" then
    error("BC is no matrix.");
  end
  [m,r] = size(BC);
  if m<r then
    error("The block column BC is wide.");
  end
  M = m/r;	// number of blocks
  if M~=round(M) then
    error("Height of the block column BC is not a integer multiple of r.");
  end

  // factorize first block
  try
    L0 = chol(BC(1:r,1:r)')';
  catch
    error(sprintf("Cholesky factorization of the first block BC(1:r,1:r) " ...
      +"failed: ""%s"".",lasterror()));
  end

  // compute the generator
  G = [L0 zeros(r,r) ; BC(r+1:$,:)/L0' BC(r+1:$,:)/L0'];

  // construct the shift matrix
  F = [spzeros(r,M*r);speye((M-1)*r,(M-1)*r) spzeros((M-1)*r,r)];

  // compute the LDL factorization of T
  [L,d] = ldl_gs(F,G,r,r);

endfunction
