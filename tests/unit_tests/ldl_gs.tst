// define tolerance
tol = 1e-12;

// create indefinite hermitian Toeplitz matrix
T = toeplitz(1:5,1:5)+toeplitz(%i*(0:4),-%i*(0:4));

// compute displacement with respect to the (complex) shift matrix
F = [spzeros(1,5);speye(4,4) spzeros(4,1)]+%i*spzeros(5,5);
D = T-F*T*F';

// compute generator
G = [T(:,1) [0;T(2:$,1)]];
J = [1 0;0 -1];

// test generator
if norm(D-G*J*G')>tol then pause,end

// compute LDL factorization
[L,d] = ldl_gs(F,G,1,1);

// test decomposition
if norm(T-L*diag(d)*L')>tol then pause,end

// repeat test with a real matrix
  
// create indefinite hermitian Toeplitz matrix
T = toeplitz(1:5,1:5);

// compute displacement with respect to the shift matrix
F = [spzeros(1,5);speye(4,4) spzeros(4,1)];
D = T-F*T*F';

// compute generator
G = [T(:,1) [0;T(2:$,1)]];
J = [1 0;0 -1];

// test generator
if norm(D-G*J*G')>tol then pause,end

// compute LDL factorization
[L,d] = ldl_gs(F,G,1,1);

// test decomposition
if norm(T-L*diag(d)*L')>tol then pause,end

// repeat with a complex shift matrix but real generator

// => checks whether the C interface routine sci_ldl_gs() correctly
// generates a missing imaginary part of the generator

// create indefinite hermitian Toeplitz matrix
T = toeplitz(1:5,1:5);

// compute displacement with respect to the (complex) shift matrix
F = [spzeros(1,5);speye(4,4) spzeros(4,1)]+%i*spzeros(5,5);
D = T-F*T*F';

// compute generator
G = [T(:,1) [0;T(2:$,1)]];
J = [1 0;0 -1];

// test generator
if norm(D-G*J*G')>tol then pause,end

// compute LDL factorization
[L,d] = ldl_gs(F,G,1,1);

// test decomposition
if norm(T-L*diag(d)*L')>tol then pause,end

// repeat with a real shift matrix but complex generator

// => checks whether the C interface routine sci_ldl_gs() correctly
// generates a missing imaginary part of the shift matrix

// create indefinite hermitian Toeplitz matrix
T = toeplitz(1:5,1:5);

// compute displacement with respect to the shift matrix
F = [spzeros(1,5);speye(4,4) spzeros(4,1)];
D = T-F*T*F';

// compute generator
G = [T(:,1) [0;T(2:$,1)]]+%i*zeros(5,2);
J = [1 0;0 -1];

// test generator
if norm(D-G*J*G')>tol then pause,end

// compute LDL factorization
[L,d] = ldl_gs(F,G,1,1);

// test decomposition
if norm(T-L*diag(d)*L')>tol then pause,end
