// define tolerance
tol = 1e-12;

// fix random number generator
rand("seed",123);

// set some parameters
M = 5;		// no. of vertical blocks
N = 5;		// no. of horizontal blocks
r = 2;		// block height
s = 2;		// block width

// create random first block column and row
BC = rand(M*r,s);
BR = rand(r,N*s);
BC(1:r,:) = BR(:,1:s);

// compute QR factorization
[Q,R] = qr_blocktoep(BC,BR);

// check Q
if norm(Q*Q'-eye())>tol then pause,end

// check that R is upper triangular
R2 = R;
[m,n] = size(R2);
for k=1:m do
  R2(k,k:$) = 0;
end
if norm(R2)>tol then pause,end

// check that T=Q*R is the block Toeplitz specified by BC and BR
T = Q*R;
for k=1:M do
  if norm(T((k-1)*r+1:k*r,(k-1)*s+1:$)-BR(:,1:$-(k-1)*s))>tol then pause,end
end
for k=1:N do
  if norm(T((k-1)*r+1:$,(k-1)*s+1:k*s)-BC(1:$-(k-1)*r,:))>tol then pause,end
end

// we repeat with some other parameters

// set some parameters
M = 4;		// no. of vertical blocks
N = 5;		// no. of horizontal blocks
r = 2;		// block height
s = 3;		// block width

// create random first block column and row
BC = rand(M*r,s);
BR = rand(r,N*s);
BC(1:r,:) = BR(:,1:s);

// compute QR factorization
[Q,R] = qr_blocktoep(BC,BR);

// check Q
if norm(Q*Q'-eye())>tol then pause,end

// check that R is upper triangular
R2 = R;
[m,n] = size(R2);
for k=1:m do
  R2(k,k:$) = 0;
end
if norm(R2)>tol then pause,end

// check that T=Q*R is the block Toeplitz specified by BC and BR
T = Q*R;
for k=1:M do
  if norm(T((k-1)*r+1:k*r,(k-1)*s+1:$)-BR(:,1:$-(k-1)*s))>tol then pause,end
end
for k=1:N do
  if norm(T((k-1)*r+1:$,(k-1)*s+1:k*s)-BC(1:$-(k-1)*r,:))>tol then pause,end
end

// we repeat with some other parameters

// set some parameters
M = 9;		// no. of vertical blocks
N = 5;		// no. of horizontal blocks
r = 5;		// block height
s = 3;		// block width

// create random first block column and row
BC = rand(M*r,s);
BR = rand(r,N*s);
BC(1:r,:) = BR(:,1:s);

// compute QR factorization
[Q,R] = qr_blocktoep(BC,BR);

// check Q
if norm(Q'*Q-eye())>tol then pause,end

// check that R is upper triangular
R2 = R;
[m,n] = size(R2);
for k=1:m do
  R2(k,k:$) = 0;
end
if norm(R2)>tol then pause,end

// check that T=Q*R is the block Toeplitz specified by BC and BR
T = Q*R;
for k=1:M do
  if norm(T((k-1)*r+1:k*r,(k-1)*s+1:$)-BR(:,1:$-(k-1)*s))>tol then pause,end
end
for k=1:N do
  if norm(T((k-1)*r+1:$,(k-1)*s+1:k*s)-BC(1:$-(k-1)*r,:))>tol then pause,end
end
