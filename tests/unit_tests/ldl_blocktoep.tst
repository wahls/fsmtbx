// define tolerance
tol = 1e-12;

// fix random number generator
rand("seed",123);

// set some parameters
M = 5;		// no. of blocks
r = 2;		// block size

// create random first block column with positive-definite first block
BC = rand(M*r,r);
BC(1:r,1:r) = BC(1:r,1:r)*BC(1:r,1:r)';

// compute LDL factorization
[L,d] = ldl_blocktoep(BC);

// check that L is lower triangular
L2 = L;
[m,n] = size(L2);
for k=1:m do
  L2(k,1:k) = 0;
end
if norm(L2)>tol then pause,end

// check that T=L*diag(d)*L' is the block Toeplitz specified by BC
T = L*diag(d)*L';
BR = BC';	// first block row of T
for k=1:M do
  if norm(T((k-1)*r+1:k*r,(k-1)*r+1:$)-BR(:,1:$-(k-1)*r))>tol then pause,end
end
for k=1:M do
  if norm(T((k-1)*r+1:$,(k-1)*r+1:k*r)-BC(1:$-(k-1)*r,:))>tol then pause,end
end
