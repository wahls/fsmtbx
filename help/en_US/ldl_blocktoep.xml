<?xml version="1.0" encoding="UTF-8"?>

<refentry version="5.0-subset Scilab" xml:id="ldl_blocktoep" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>ldl_blocktoep</refname>
    <refpurpose>LDL factorization of a strongly regular hermitian block Toeplitz
      matrix with positive-definite first block</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[L,d] = ldl_blocktoep(BC)</synopsis>
  </refsynopsisdiv>

   <refsection>
    <title>Parameters</title>
      <variablelist>
         <varlistentry>
            <term>BC</term>
            <listitem>
               <para>M*r x r matrix</para>
            </listitem>
         </varlistentry>
         <varlistentry>
            <term>L</term>
            <listitem>
               <para>lower triangular M*r x M*r matrix</para>
            </listitem>
         </varlistentry>
         <varlistentry>
            <term>d</term>
            <listitem>
	      <para>M*r x 1 vector, entries are +1 or -1</para>
            </listitem>
         </varlistentry>
      </variablelist>
   </refsection>

  <refsection>
    <title>Description</title>

    <para>The routine computes the LDL factorization of a hermitian but
    possibly indefinite block Toeplitz matrix T=[T(0) T(1)' T(2)' ... T(M-1)';
    T(1) T(0) T(1)' ... T(M-2)'; ... ;T(M-1) T(M-2) T(M-3) ... T(0)] with first
    block column BC=[T(0);T(1);...;T(M-1)]. Here, the blocks T(0),...,T(M-1) are
    r x r matrices. We assume that the first block T(0)=T(0)'=BC(1:r,1:r)
    is hermitian and positive-definite and that the matrix T is strongly regular
    (i.e., all leading minors are non-zero). The algorithm computes a lower
    triangular matrix L and a vector d with entries +1 / -1 such that
    T=L*diag(d)*L'.</para>

  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">
BC = (1:5)'+%i*(0:4)';		// first column with BC(1,1)=BC(1,1)'>0
T = toeplitz(BC,BC');		// corresponding Toeplitz matrix
[L,d] = ldl_blocktoep(BC);	// perform LDL factorization
disp(clean(T-L*diag(d)*L'));	// show residual; should be zero</programlisting>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Sander WAHLS</member>
    </simplelist>
  </refsection>
</refentry>
