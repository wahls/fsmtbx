#ifndef __COMPLEX_OD_METHOD_NEG_H
#define __COMPLEX_OD_METHOD_NEG_H

#include <math.h>
#include <doublecomplex.h>

void complex_od_method_neg(doublecomplex *x, doublecomplex *y, \
  doublecomplex al, doublecomplex be, const unsigned int N);

#endif