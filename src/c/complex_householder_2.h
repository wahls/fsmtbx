#ifndef __COMPLEX_HOUSEHOLDER_2_H
#define __COMPLEX_HOUSEHOLDER_2_H

#include <string.h>
#include <math.h>
#include <doublecomplex.h>

void complex_householder_2(doublecomplex *v, const unsigned int N);

#endif