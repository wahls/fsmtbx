#include "complex_sparse_matvecmult.h"

/**
 * Computes the complex matrix-vector product y=A*x of a sparse m x m matrix A
 * and a m x 1 column vector x.
 * 
 * @param m Size of the matrix
 * @param piNbItemRow Number of non-zero elements in each row of the matrix
 * @param piColPos Columns of the non-zero elements of the matrix
 * @param pdblReal Real parts of the non-zero elements of the matrix
 * @param pdblImg Imaginary parts of the non-zero elements of the matrix
 * @param x Vector x=[xi] to be multiplied with A; is stored in the format
 * 	real(x1),imag(x1),real(x2),imag(x2),...
 * @param y Result y=[yi] of the multiplication; is stored in the format
 * 	real(y1),imag(y1),real(y2),imag(y2),...
 */
void complex_sparse_matvecmult(int m, int *piNbItemRow, int *piColPos, \
  double *pdblReal, double *pdblImg, doublecomplex *x, doublecomplex *y)
{
  int i,j,k;
  
  k = 0;
  for (i=0; i<m; i++)
  {
    y[i].r = 0;
    y[i].i = 0;
    for (j=0; j<piNbItemRow[i]; j++)
    {
      y[i].r += pdblReal[k]*x[piColPos[k]-1].r-pdblImg[k]*x[piColPos[k]-1].i;
      y[i].i += pdblReal[k]*x[piColPos[k]-1].i+pdblImg[k]*x[piColPos[k]-1].r;
      k++;
    }
  }
}