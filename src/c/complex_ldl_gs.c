#include "complex_ldl_gs.h"

/**
 * Generalized Schur algorithm for strongly regular complex hermitian matrices
 * with displacement structure of Stein type.
 *
 * The routine implements Algorithm 3.1.1 in T. Kailath and A. Sayed (eds):
 * "Fast Reliable Algorithms for Matrices with Structure", SIAM, 1999. Given
 * a structured N x N matrix A, a strictly lower triangular N x N matrix F and
 * a N x r generator matrix G such that the displacement A-F*A*F'=G*J*G' is of
 * low rank, the algorithm computes a lower triangular N x N matrix L and a
 * N x 1 vector d consisting of +1 and -1 such that A=L*diag(d)*L'. Here,
 * J=[eye(p,p) zeros(p,q);zeros(q,p) -eye(q,q)] denotes a signature matrix and
 * r=q+p. All matrices are assumed to be complex and A has to be strongly
 * regular (i.e., all leading minors are non-zero). F is assumed sparse.
 * 
 * @param piNbItemRow Number of non-zero elements in each row of F
 * @param piColPos Columns of the non-zero elements of F
 * @param pdblReal Real parts of the non-zero elements of F
 * @param pdblImg Imaginary parts of the non-zero elements of F
 * @param G Matrix G=[gij] stored column-wise in the format real(g11),imag(g11),
 * 	real(g12),imag(g12),...
 * @param p Number of +1 elements in the signature matrix J
 * @param q Number of -1 elements in the signature matrix J
 * @param N Size of A,L,...
 * @param K Number of columns of L which actually have to be computed; may be
 * 	used for early abortion of the algorithm
 * @param LReal Real parts of the first K columns of L stored column-wise
 * @param LImg Imaginary parts of the first K columns of L stored column-wise
 * @param d First K elements of the vector d
 * 
 * @return 0=success, 1=insufficient memory, 2=numerical problem
 */
int complex_ldl_gs(int *piNbItemRow, int *piColPos, double *pdblReal, \
  double *pdblImg, doublecomplex *G, const unsigned int p, \
  const unsigned int q, const unsigned int N, const unsigned int K, \
  double *LReal, double *LImg, double *d)
{
  doublecomplex *cTmp;
  double tmp;
  unsigned int i,j,m;

  // allocate memory for a buffer
  m = (p>=q ? p : q);
  m = (m>=N ? m : N); 
  cTmp = malloc(sizeof(doublecomplex)*m);
  if (cTmp==NULL)
  {
    return 1;
  }
  
  for (i=0; i<K; i++)
  {
    // tmp <- G(i,1:p)*G(i,1:p)'-G(i,p+1:p+q)*G(i,p+1:p+q)'
    tmp = 0;
    for (j=0; j<p; j++)
    {
      tmp += G[j*N+i].r*G[j*N+i].r+G[j*N+i].i*G[j*N+i].i;
    }
    for (j=p; j<p+q; j++)
    {
      tmp -= G[j*N+i].r*G[j*N+i].r+G[j*N+i].i*G[j*N+i].i;
    }
   
    if (tmp>0)			// positive step
    {
      // apply a J-unitary transformation such that the generator
      // is in proper form with top row [*,0,...,0]
      complex_hyperbol_pos(G,p,q,N,i,cTmp);

      // extract the current column of L
      for (j=0; j<i; j++)
      {
	LReal[i*N+j] = 0;
	LImg[i*N+j] = 0;
      }
      for (j=i; j<N; j++)
      {
	LReal[i*N+j] = G[j].r;
	LImg[i*N+j] = G[j].i;
      }

      // update the generator, G(i:N,1) <- F(i:N,:)*G(:,1)
      complex_sparse_matvecmult(N,piNbItemRow,piColPos,pdblReal,pdblImg,&G[0], \
	cTmp);
      for (m=i; m<N; m++)
      {
	G[m] = cTmp[m];
      }   

      // set according element of the diagonal matrix
      d[i] = 1;
    }
    else if (tmp<0)		// negative step
    {
      // apply a J-unitary transformation such that the generator
      // is in proper form with top row [0,...,0,*]
      complex_hyperbol_neg(G,p,q,N,i,cTmp);

      // extract the current column of L
      for (j=0; j<i; j++)
      {
	LReal[i*N+j] = 0;
	LImg[i*N+j] = 0;
      }
      for (j=i; j<m; j++)
      {
	LReal[i*N+j] = G[N*(p+q-1)+j].r;
	LImg[i*N+j] = G[N*(p+q-1)+j].i;
      }

      // update the generator, G(i:N,$) <- F(i:N,:)*G(:,$)
      complex_sparse_matvecmult(N,piNbItemRow,piColPos,pdblReal,pdblImg, \
	&G[N*(p+q-1)], cTmp);
      for (m=i; m<N; m++)
      {
	G[N*(p+q-1)+m] = cTmp[m];
      }

      // set according element of the diagonal matrix
      d[i] = -1;
    }
    else			// should not happen
    {
      free(cTmp);
      return 2;
    }
  }

  free(cTmp);
  return 0;
}
