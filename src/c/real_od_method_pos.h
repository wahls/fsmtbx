#ifndef __REAL_OD_METHOD_POS_H
#define __REAL_OD_METHOD_POS_H

#include <math.h>

void real_od_method_pos(double *x, double *y, double al, double be, \
  const unsigned int N);

#endif