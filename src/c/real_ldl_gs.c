#include "real_ldl_gs.h"

/**
 * Generalized Schur algorithm for strongly regular real symmetric matrices with
 * displacement structure of Stein type.
 *
 * The routine implements Algorithm 3.1.1 in T. Kailath and A. Sayed (eds):
 * "Fast Reliable Algorithms for Matrices with Structure", SIAM, 1999. Given
 * a structured N x N matrix A, a strictly lower triangular N x N matrix F and
 * a N x r generator matrix G such that the displacement A-F*A*F'=G*J*G' is of
 * low rank, the algorithm computes a lower triangular N x N matrix L and a
 * N x 1 vector d consisting of +1 and -1 such that A=L*diag(d)*L'. Here,
 * J=[eye(p,p) zeros(p,q);zeros(q,p) -eye(p,p)] denotes a signature matrix and
 * r=q+p. All matrices are assumed to be real and A has to be strongly regular
 * (i.e., all leading minors are non-zero). F is assumed sparse.
 * 
 * @param piNbItemRow Number of non-zero elements in each row of F
 * @param piColPos Columns of the non-zero elements of F
 * @param pdbl Non-zero elements of F
 * @param G Matrix G=[gij] stored column-wise in the format g11,g12,...
 * @param p Number of +1 elements in the signature matrix J
 * @param q Number of -1 elements in the signature matrix J
 * @param N Size of A,L,...
 * @param K Number of columns of L which actually have to be computed; may be
 * 	used for early abortion of the algorithm
 * @param L First K columns of L, stored column-wise
 * @param d First K elements of the vector d
 * 
 * @return 0=success, 1=insufficient memory, 2=numerical problem
 */
int real_ldl_gs(int *piNbItemRow, int *piColPos, double *pdbl, \
  double *G, const unsigned int p, const unsigned int q, const unsigned int N, \
  const unsigned int K, double *L, double *d)
{
  double *dTmp;
  double tmp;
  unsigned int i,j,m;

  // allocate memory for a buffer
  m = (p>=q ? p : q);
  m = (m>=N ? m : N); 
  dTmp = malloc(sizeof(double)*m);
  if (dTmp==NULL)
  {
    return 1;
  }

  for (i=0; i<K; i++)
  {
    // tmp <- G(i,1:p)*G(i,1:p)'-G(i,p+1:p+q)*G(i,p+1:p+q)'
    tmp = 0;
    for (j=0; j<p; j++)
    {
      tmp += G[j*N+i]*G[j*N+i];
    }
    for (j=p; j<p+q; j++)
    {
      tmp -= G[j*N+i]*G[j*N+i];    
    }

    if (tmp>0)			// positive step
    {
      // apply a J-unitary transformation such that the generator
      // is in proper form with top row [*,0,...,0]
      real_hyperbol_pos(G,p,q,N,i,dTmp);

      // extract the current column of L
      for (j=0; j<i; j++)
      {
	L[i*N+j] = 0;
      }
      for (j=i; j<N; j++)
      {
	L[i*N+j] = G[j];
      }

      // update the generator, G(i:N,1) <- F(i:N,:)*G(:,1)
      real_sparse_matvecmult(N,piNbItemRow,piColPos,pdbl,&G[0],dTmp);
      for (m=i; m<N; m++)
      {
	G[m] = dTmp[m];
      }   

      // set according element of the diagonal matrix
      d[i] = 1;
    }
    else if (tmp<0)		// negative step
    {
      // apply a J-unitary transformation such that the generator
      // is in proper form with top row [0,...,0,*]
      real_hyperbol_neg(G,p,q,N,i,dTmp);
     
      // extract the current column of L
      for (j=0; j<i; j++)
      {
	L[i*N+j] = 0;
      }
      for (j=i; j<m; j++)
      {
	L[i*N+j] = G[N*(p+q-1)+j];
      }

      // update the generator, G(i:N,$) <- F(i:N,:)*G(:,$)
      real_sparse_matvecmult(N,piNbItemRow,piColPos,pdbl,&G[N*(p+q-1)],dTmp);
      for (m=i; m<N; m++)
      {
	G[N*(p+q-1)+m] = dTmp[m];
      }

      // set according element of the diagonal matrix
      d[i] = -1;
    }
    else			// should not happen
    {
      free(dTmp);
      return 2;
    }
  }

  free(dTmp);
  return 0;
}
