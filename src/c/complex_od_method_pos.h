#ifndef __COMPLEX_OD_METHOD_POS_H
#define __COMPLEX_OD_METHOD_POS_H

#include <math.h>
#include <doublecomplex.h>

void complex_od_method_pos(doublecomplex *x, doublecomplex *y, \
  doublecomplex al, doublecomplex be, const unsigned int N);

#endif