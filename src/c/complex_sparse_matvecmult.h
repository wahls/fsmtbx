#ifndef __COMPLEX_SPARSE_MATVECMULT_H
#define __COMPLEX_SPARSE_MATVECMULT_H

#include <doublecomplex.h>

void complex_sparse_matvecmult(int m, int *piNbItemRow, int *piColPos, \
  double *pdblReal, double *pdblImg, doublecomplex *x, doublecomplex *y);

#endif