#include "complex_hyperbol_pos.h"

/**
 * Applies a J-unitary transformation such that the first element is the only
 * non-zero element in the first row of the N x (p+q) generator matrix G.
 *
 * Here, J=[eye(p,p) zeros(p,q);zeros(q,p) -eye(q,q)].
 * 
 * @param G Generator matrix G=[gij] stored column-wise in the format real(g11),
 * 	imag(g11),real(g12),imag(g12),...
 * @param p Number of +1 elements in the signature matrix J
 * @param q Number of -1 elements in the signature matrix J
 * @param N Height of the generator matrix G
 * @param M Specifies which row of G should be considered the first row (use if
 * 	the matrix to be transformed is a submatrix of the actual matrix in the
 * 	memory)
 * @param v Buffer, size should be at least max(p,q)*sizeof(doublecomplex)
 */
void complex_hyperbol_pos(doublecomplex *G, const unsigned int p, \
  const unsigned int q, const unsigned int N, const unsigned int M, \
  doublecomplex *v)
{
  doublecomplex tmp,al,be;
  double sq_nrm;
  unsigned int i,j;

  // set v <- G(M,1:p)'
  for (j=0; j<p; j++)
  {
    v[j].r = G[j*N+M].r;
    v[j].i = -G[j*N+M].i;
  }

  // compute the according Householder vector
  complex_householder_1(v,p);

  // squared norm of the Householder vector
  sq_nrm = 0;
  for (j=0; j<p; j++)
  {
    sq_nrm += v[j].r*v[j].r+v[j].i*v[j].i;
  }

  // apply the Householder vector to the first p columns of G
  // -> M-th row of G is of the form [*,0,...0,*,...,*,*]
  for (i=0; i<N-M; i++)
  {
    tmp.r = 0;
    tmp.i = 0;
    for (j=0; j<p; j++)			// tmp <- 2*G(i,1:p)*v/(v'*v)
    {
      tmp.r += 2*(G[j*N+(M+i)].r*v[j].r-G[j*N+(M+i)].i*v[j].i)/sq_nrm;
      tmp.i += 2*(G[j*N+(M+i)].r*v[j].i+G[j*N+(M+i)].i*v[j].r)/sq_nrm;
    }
    for (j=0; j<p; j++)			// G(i,1:p) <- G(i,1:p)-tmp*v'
    {
      G[j*N+(M+i)].r -= tmp.r*v[j].r+tmp.i*v[j].i;
      G[j*N+(M+i)].i -= -tmp.r*v[j].i+tmp.i*v[j].r;
    }
  }

  // set v <- G(M,p+1:p+q)'
  for (j=0; j<q; j++)
  {
    v[j].r = G[(p+j)*N+M].r;
    v[j].i = -G[(p+j)*N+M].i;
  }

  // compute the according Householder vector
  complex_householder_2(v,q);

  // squared norm of the Householder vector
  sq_nrm = 0;
  for (j=0; j<q; j++)
  {
    sq_nrm += v[j].r*v[j].r+v[j].i*v[j].i;
  }

  // apply the Householder vector to the last q columns of G
  // -> M-th row of G is of the form [*,0,...0,0,...,0,*]
  for (i=0; i<N-M; i++)
  {
    tmp.r = 0;
    tmp.i = 0;
    for (j=0; j<q; j++)			// tmp <- 2*G(i,p+1:p+q)*v/(v'*v)
    {
      tmp.r += 2*(G[(p+j)*N+(M+i)].r*v[j].r-G[(p+j)*N+(M+i)].i*v[j].i)/sq_nrm;
      tmp.i += 2*(G[(p+j)*N+(M+i)].r*v[j].i+G[(p+j)*N+(M+i)].i*v[j].r)/sq_nrm;
    }
    for (j=0; j<q; j++)			// G(i,1:p) <- G(i,p+1:p+q)-tmp*v'
    {
      G[(p+j)*N+(M+i)].r -= tmp.r*v[j].r+tmp.i*v[j].i;
      G[(p+j)*N+(M+i)].i -= -tmp.r*v[j].i+tmp.i*v[j].r;
    }
  }

  // apply the Hyperbolic rotation
  // -> M-th row of G is of the form [*,0,...0,0,...,0,0]
  complex_od_method_pos(&G[M], &G[(p+q-1)*N+M], G[M], G[(p+q-1)*N+M], N-M);
}
