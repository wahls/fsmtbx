#ifndef __COMPLEX_HYPERBOL_NEG_H
#define __COMPLEX_HYPERBOL_NEG_H

#include <stdlib.h>
#include <complex_householder_1.h>
#include <complex_householder_2.h>
#include <complex_od_method_neg.h>

void complex_hyperbol_neg(doublecomplex *G, const unsigned int p, \
  const unsigned int q, const unsigned int N, const unsigned int M, \
  doublecomplex *v);

#endif