#include "real_householder_2.h"

/**
 * Given a N x 1 vector v, the routine computes a non-zero N x 1 vector w such
 * that (eye(N,N)-2*w*w'/(w'*w))*v is a multiple of the last unit vector.
 * 
 * @param v Real vector v=[vi] in format v1,v2,... ; is overwritten with the
 * 	result w
 * @param N Length of v and w
 */
void real_householder_2(double *v, const unsigned int N)
{
  double nrm;
  int k;

  // compute the norm of v (TODO: avoid under-/overflows)
  nrm = 0;
  for (k=0; k<N; k++)
  {
    nrm += v[k]*v[k];
  }
  nrm = sqrt(nrm);

  // compute the Householder vector
  if (nrm>0)
  {
    // if v~=0 then compute the Householder vector in the standard way
    for (k=0; k<N; k++)
    {
      v[k] = v[k]/nrm;
    }
    nrm = sqrt(v[N-1]*v[N-1]);
    if (nrm>0)
    {
      v[N-1] += v[N-1]/nrm;
    }
    else
    {
      v[N-1] = 1;
    }
  }
  else
  {
    // we have v=0 and the standard method breaks down; since now any vector
    // w~=0 is a Householder vector [in the sense that 0=(I-2*w*w'/(w'*w))*v
    // is a multiple of the last unit vector] we choose w=(first unit vector)
    // because then 1/(w'*w) stays well-defined
    memset(v,0,sizeof(double)*N);
    v[0] = 1;
  }
}
