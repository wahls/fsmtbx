#include "complex_od_method_neg.h"

/**
 * Computes the hyperbolic rotation [xx,yy]=[x,y]*[1 -rho;-rho' 1]/scl where
 * x,y,xx,yy are N x 1 column vectors, rho=al'/be', and scl=sqrt(1-abs(rho)^2).
 *
 * We assume that abs(be)>abs(al).
 * 
 * @param x Vector x=[xi] in the format real(x1),imag(x1),real(x2),imag(x2)... ;
 * 	is overwritten with xx
 * @param y Vector y=[yi] in the format real(y1),imag(y1),real(y2),imag(y2)... ;
 * 	is overwritten with yy
 * @param al Specifies the hyperbolic rotation
 * @param be Specifies the hyperbolic rotation
 * @param N Size of the vectors x,y,xx,yy
 */
void complex_od_method_neg(doublecomplex *x, doublecomplex *y, \
  doublecomplex al, doublecomplex be, const unsigned int N)
{
  doublecomplex z;
  double dTmp1,dTmp2;
  unsigned int i;

  // catch the trivial case al=0 which breaks the algorithm
  if ((al.r==0)&&(al.i==0))
  {
    return;
  }

  // note: we reuse al and be as tmp variables

  // compute z <- (be'/abs(be))/(al'/abs(al));
  dTmp1 = sqrt(be.r*be.r+be.i*be.i);
  be.r = be.r/dTmp1;
  be.i = -be.i/dTmp1;
  dTmp2 = sqrt(al.r*al.r+al.i*al.i);
  al.r = al.r/dTmp2;
  al.i = -al.i/dTmp2;
  z.r = be.r*al.r+be.i*al.i;		// z <- be/al; note: we can skip divison
  z.i = be.i*al.r-be.r*al.i;		// by abs(al)^2 because abs(al)=1

  // compute dTmp1 <- sqrt((abs(be)+abs(al))/(abs(be)-abs(al)))
  dTmp1 = sqrt((dTmp1+dTmp2)/(dTmp1-dTmp2));
 
  for (i=0; i<N; i++)
  {
    // compute be <- -x+z*y and al <- -x-z*y
    be.r = z.r*y[i].r-z.i*y[i].i;			// be <- z*y
    be.i = z.r*y[i].i+z.i*y[i].r;
    al.r = -be.r;					// al <- -z*y;
    al.i = -be.i;
    be.r = be.r-x[i].r;
    be.i = be.i-x[i].i;
    al.r = al.r-x[i].r;
    al.i = al.i-x[i].i;

    // compute be <- 0.5*be*tmp and al = 0.5*al/tmp
    be.r = 0.5*be.r*dTmp1;
    be.i = 0.5*be.i*dTmp1;
    al.r = 0.5*al.r/dTmp1;
    al.i = 0.5*al.i/dTmp1;

    // compute xx <- -be-al
    x[i].r = -be.r-al.r;
    x[i].i = -be.i-al.i;

    // compute yy <- z'*(be-al)
    y[i].r = z.r*(be.r-al.r)+z.i*(be.i-al.i);
    y[i].i = z.r*(be.i-al.i)-z.i*(be.r-al.r);
  }
}
