#ifndef __REAL_HOUSEHOLDER_1_H
#define __REAL_HOUSEHOLDER_1_H

#include <string.h>
#include <math.h>

void real_householder_1(double *v, const unsigned int N);

#endif