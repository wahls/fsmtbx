#include "real_od_method_neg.h"

/**
 * Computes the hyperbolic rotation [xx,yy]=[x,y]*[1 -rho;-rho 1]/scl where
 * x,y,x1,y1 are N x 1 column vectors, rho=al/be, and scl=sqrt(1-abs(rho)^2).
 *
 * We assume that abs(be)>abs(al).
 * 
 * @param x Vector x=[xi] in the format x1,x2,... ; is overwritten with xx
 * @param y Vector y=[yi] in the format y1,y2,... ; is overwritten with yy
 * @param al Specifies the hyperbolic rotation
 * @param be Specifies the hyperbolic rotation
 * @param N Size of the vectors x,y,xx,yy
 */
void real_od_method_neg(double *x, double *y, double al, double be, \
  const unsigned int N)
{
  double dTmp;
  unsigned int i;

  // catch the trivial case al=0 which breaks the algorithm
  if (al==0)
  {
    return;
  }

  // compute dTmp <- sqrt((abs(al)+abs(be))/(abs(al)-abs(be)))
  dTmp = sqrt((be+al)/(be-al));

  for (i=0; i<N; i++)
  {
    // note: we reuse al and be as tmp variables

    // compute al <- x-y and be <- x+y
    al = x[i]-y[i];
    be = x[i]+y[i];

    // compute al <- 0.5*al*dTmp and be <- 0.5*be/dTmp
    al = 0.5*al*dTmp;
    be = 0.5*be/dTmp;

    // compute xx <- al+be
    x[i] = al+be;

    // compute yy <- -al+be
    y[i] = -al+be;
  }
}
