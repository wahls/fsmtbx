#include "complex_householder_2.h"

/**
 * Given a N x 1 vector v, the routine computes a non-zero N x 1 vector w such
 * that (eye(N,N)-2*w*w'/(w'*w))*v is a multiple of the last unit vector.
 * 
 * @param v Complex vector v=[vi] in format real(v1),imag(v1),real(v2),
 * 	imag(v2),... ; is overwritten with the result w
 * @param N Length of v and w
 */
void complex_householder_2(doublecomplex *v, const unsigned int N)
{
  double nrm;
  int k;

  // compute the norm of v (TODO: avoid under-/overflows)
  nrm = 0;
  for (k=0; k<N; k++)
  {
    nrm += v[k].r*v[k].r+v[k].i*v[k].i;
  }
  nrm = sqrt(nrm);

  // compute the Householder vector
  if (nrm>0)
  {
    // if v~=0 then compute the Householder vector in the standard way
    for (k=0; k<N; k++)
    {
      v[k].r = v[k].r/nrm;
      v[k].i = v[k].i/nrm;
    }
    nrm = sqrt(v[N-1].r*v[N-1].r+v[N-1].i*v[N-1].i);
    if (nrm>0)
    {
      v[N-1].r += v[N-1].r/nrm;
      v[N-1].i += v[N-1].i/nrm;
    }
    else
    {
      v[N-1].r = 1;
      v[N-1].i = 0;
    }
  }
  else
  {
    // we have v=0 and the standard method breaks down; since now any vector
    // w~=0 is a Householder vector [in the sense that 0=(I-2*w*w'/(w'*w))*v
    // is a multiple of the last unit vector] we choose w=(first unit vector)
    // because then 1/(w'*w) stays well-defined
    memset(v,0,sizeof(doublecomplex)*N);
    v[0].r = 1;
  }
}
