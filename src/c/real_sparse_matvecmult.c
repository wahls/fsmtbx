#include "real_sparse_matvecmult.h"

/**
 * Computes the real matrix-vector product y=A*x of a sparse m x m matrix A and 
 * a m x 1 column vector x.
 * 
 * @param m Size of the matrix
 * @param piNbItemRow Number of non-zero elements in each row of the matrix
 * @param piColPos Columns of the non-zero elements of the matrix
 * @param pdbl Non-zero elements of the matrix
 * @param x Vector x=[xi] to be multiplied with A; is stored in the format
 * 	x1,x2,...
 * @param y Result y=[yi] of the multiplication; is stored in the format
 * 	y1,y2,...
 */
void real_sparse_matvecmult(int m, int *piNbItemRow, int *piColPos, \
  double *pdbl, double *x, double *y)
{
  int i,j,k;
  
  k = 0;
  for (i=0; i<m; i++)
  {
    y[i] = 0;
    for (j=0; j<piNbItemRow[i]; j++)
    {
      y[i] += pdbl[k]*x[piColPos[k]-1];
      k++;
    }
  }
}