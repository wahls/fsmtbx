#ifndef __REAL_LDL_GS_H
#define __REAL_LDL_GS_H

#include <stdlib.h>
#include <real_hyperbol_pos.h>
#include <real_hyperbol_neg.h>
#include <real_sparse_matvecmult.h>

int real_ldl_gs(int *piNbItemRow, int *piColPos, double *pdbl, \
  double *G, const unsigned int p, const unsigned int q, const unsigned int N, \
  const unsigned int K, double *L, double *d);

#endif