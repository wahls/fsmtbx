#include "real_hyperbol_pos.h"

/**
 * Applies a J-unitary transformation such that the first element is the only
 * non-zero element in the first row of the N x (q+p) generator matrix G.
 *
 * Here, J=[eye(p,p) zeros(p,q);zeros(q,p) -eye(q,q)].
 * 
 * @param G Generator matrix G=[gij] stored column-wise in the format g11,g12,
 * 	...
 * @param p Number of +1 elements in the signature matrix J
 * @param q Number of -1 elements in the signature matrix J
 * @param N Height of the generator matrix G
 * @param M Specifies which row of G should be considered the first row (use if
 * 	the matrix to be transformed is a submatrix of the actual matrix in the
 * 	memory)
 * @param v Buffer, size should be at least max(p,q)
 */
void real_hyperbol_pos(double *G, const unsigned int p, \
  const unsigned int q, const unsigned int N, const unsigned int M, \
  double *v)
{
  double tmp,al,be;
  double sq_nrm;
  unsigned int i,j;

  // set v <- G(M,1:p)'
  for (j=0; j<p; j++)
  {
    v[j] = G[j*N+M];
  }

  // compute the according Householder vector
  real_householder_1(v,p);

  // squared norm of the Householder vector
  sq_nrm = 0;
  for (j=0; j<p; j++)
  {
    sq_nrm += v[j]*v[j];
  }

  // apply the Householder vector to the first p columns of G
  // -> M-th row of G is of the form [*,0,...0,*,...,*,*]
  for (i=0; i<N-M; i++)
  {
    tmp = 0;
    for (j=0; j<p; j++)			// tmp <- 2*G(i,1:p)*v/(v'*v)
    {
      tmp += 2*G[j*N+(M+i)]*v[j]/sq_nrm;
    }
    for (j=0; j<p; j++)			// G(i,1:p) <- G(i,1:p)-tmp*v'
    {
      G[j*N+(M+i)] -= tmp*v[j];
    }
  }

  // set v <- G(M,p+1:p+q)'
  for (j=0; j<q; j++)
  {
    v[j] = G[(p+j)*N+M];
  }

  // compute the according Householder vector
  real_householder_2(v,q);

  // squared norm of the Householder vector
  sq_nrm = 0;
  for (j=0; j<q; j++)
  {
    sq_nrm += v[j]*v[j];
  }

  // apply the Householder vector to the last q columns of G
  // -> M-th row of G is of the form [*,0,...0,0,...,0,*]
  for (i=0; i<N-M; i++)
  {
    tmp = 0;
    for (j=0; j<q; j++)			// tmp <- 2*G(i,p+1:p+q)*v/(v'*v)
    {
      tmp += 2*G[(p+j)*N+(M+i)]*v[j]/sq_nrm;
    }
    for (j=0; j<q; j++)			// G(i,1:p) <- G(i,p+1:p+q)-tmp*v'
    {
      G[(p+j)*N+(M+i)] -= tmp*v[j];
    }
  }

  // apply the Hyperbolic rotation
  // -> M-th row of G is of the form [*,0,...0,0,...,0,0]
  real_od_method_pos(&G[M], &G[(p+q-1)*N+M], G[M], G[(p+q-1)*N+M], N-M);
}
