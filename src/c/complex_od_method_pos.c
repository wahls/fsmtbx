#include "complex_od_method_pos.h"

/**
 * Computes the hyperbolic rotation [xx,yy]=[x,y]*[1 -rho;-rho' 1]/scl where
 * x,y,xx,yy are N x 1 column vectors, rho=be/al, and scl=sqrt(1-abs(rho)^2).
 *
 * We assume that abs(al)>abs(be).
 * 
 * @param x Vector x=[xi] in the format real(x1),imag(x1),real(x2),imag(x2)... ;
 *	is overwritten with xx
 * @param y Vector y=[yi] in the format real(y1),imag(y1),real(y2),imag(y2)... ;
 *	is overwritten with yy
 * @param al Specifies the hyperbolic rotation
 * @param be Specifies the hyperbolic rotation
 * @param N Size of the vectors x,y,xx,yy
 */
void complex_od_method_pos(doublecomplex *x, doublecomplex *y, \
  doublecomplex al, doublecomplex be, const unsigned int N)
{
  doublecomplex z;
  double dTmp1,dTmp2;
  unsigned int i;

  // catch the trivial case be=0 which breaks the algorithm
  if ((be.r==0)&&(be.i==0))
  {
    return;
  }

  // note: we reuse al and be as tmp variables

  // compute z <- (al'/abs(al))/(be'/abs(be));
  dTmp1 = sqrt(al.r*al.r+al.i*al.i);
  al.r = al.r/dTmp1;
  al.i = -al.i/dTmp1;
  dTmp2 = sqrt(be.r*be.r+be.i*be.i);
  be.r = be.r/dTmp2;
  be.i = -be.i/dTmp2;
  z.r = al.r*be.r+al.i*be.i;		// z <- al/be; note: we can skip divison
  z.i = al.i*be.r-al.r*be.i;		// by abs(be)^2 because abs(be)=1

  // compute dTmp1 <- sqrt((abs(al)+abs(be))/(abs(al)-abs(be)))
  dTmp1 = sqrt((dTmp1+dTmp2)/(dTmp1-dTmp2));
  
  for (i=0; i<N; i++)
  {
    // compute al <- z*x-y and be <- -z*x-y
    al.r = z.r*x[i].r-z.i*x[i].i;			// al <- z*x
    al.i = z.r*x[i].i+z.i*x[i].r;
    be.r = -al.r;					// be <- -z*x;
    be.i = -al.i;
    al.r = al.r-y[i].r;
    al.i = al.i-y[i].i;
    be.r = be.r-y[i].r;
    be.i = be.i-y[i].i;

    // compute al <- 0.5*al*tmp and be <- 0.5*be/tmp
    al.r = 0.5*al.r*dTmp1;
    al.i = 0.5*al.i*dTmp1;
    be.r = 0.5*be.r/dTmp1;
    be.i = 0.5*be.i/dTmp1;

    // compute xx <- z'*(al-be)
    x[i].r = z.r*(al.r-be.r)+z.i*(al.i-be.i);
    x[i].i = z.r*(al.i-be.i)-z.i*(al.r-be.r);

    // compute yy <- -al-be
    y[i].r = -al.r-be.r;
    y[i].i = -al.i-be.i;
  }
}
