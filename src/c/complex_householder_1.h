#ifndef __COMPLEX_HOUSEHOLDER_1_H
#define __COMPLEX_HOUSEHOLDER_1_H

#include <string.h>
#include <math.h>
#include <doublecomplex.h>

void complex_householder_1(doublecomplex *v, const unsigned int N);

#endif