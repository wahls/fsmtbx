#ifndef __REAL_HYPERBOL_POS_H
#define __REAL_HYPERBOL_POS_H

#include <stdlib.h>
#include <real_householder_1.h>
#include <real_householder_2.h>
#include <real_od_method_pos.h>

void real_hyperbol_pos(double *G, const unsigned int p, \
  const unsigned int q, const unsigned int N, const unsigned int M, \
  double *v);

#endif