#ifndef __REAL_OD_METHOD_NEG_H
#define __REAL_OD_METHOD_NEG_H

#include <math.h>

void real_od_method_neg(double *x, double *y, double al, double be, \
  const unsigned int N);

#endif