#ifndef __COMPLEX_LDL_GS_H
#define __COMPLEX_LDL_GS_H

#include <stdlib.h>
#include <Scierror.h>
#include <complex_hyperbol_pos.h>
#include <complex_hyperbol_neg.h>
#include <complex_sparse_matvecmult.h>

int complex_ldl_gs(int *piNbItemRow, int *piColPos, double *pdblReal, \
  double *pdblImg, doublecomplex *G, const unsigned int p, \
  const unsigned int q, const unsigned int N, const unsigned int K, \
  double *LReal, double *LImg, double *d);

#endif