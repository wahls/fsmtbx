#ifndef __REAL_SPARSE_MATVECMULT_H
#define __REAL_SPARSE_MATVECMULT_H

void real_sparse_matvecmult(int m, int *piNbItemRow, int *piColPos, \
  double *pdbl, double *x, double *y);

#endif