#ifndef __REAL_HOUSEHOLDER_2_H
#define __REAL_HOUSEHOLDER_2_H

#include <string.h>
#include <math.h>

void real_householder_2(double *v, const unsigned int N);

#endif