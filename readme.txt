CONTENT

This archive contains the Factorization of Structured Matrices Toolbox for
Scilab (see http://fsmtbx.origo.ethz.ch for more information).

INSTALLATION

1. Extract the contents of this archive into some directory, e.g.,
  /home/john/fsmtbx or c:\fsmtbx

2. Run Scilab (version 5.2 or higher)

3. Build the toolbox by executing the command
  <exec /home/john/fsmtbx/builder.sce> (replace the directory with your chosen
  path; do not type the "<" and ">").

Note: Installation requires a working C compiler which is recognized by Scilab.
You may want to customize the CFLAGS variable in the file src/c/builder_c.sce
before building the toolbox. E.g., if you are using the GCC compiler, you may
want to add the -O3 flag in order to enable advanced optimizations. 

USAGE

The toolbox has to be loaded with the command
<exec /home/john/fsmtbx/loader.sce> (replace the directory with your chosen
path) before it can be used. This process can be automated by changing the
Scilab startup script. Run the Scilab commands <help ldl_gs>,
<help ldl_blocktoep> and <help qr_blocktoep> to get more information on how to
use the toolbox.