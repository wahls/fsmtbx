#include "stack-c.h" 
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "complex_ldl_gs.h"
#include "real_ldl_gs.h"

/**
 * Gateway function which allows Scilab to interface the real_ldl_gs() and
 * complex_ldl_gs() C routines.
 * 
 * @return -n=problem with parameter n, 0=success, 1=insufficient memory,
 * 	2=numerical problems
 */
int sci_ldl_gs(char *fname)
{
  SciErr sciErr;

  // first parameter: sparse matrix F
  int m1 = 0, n1 = 0, items1 = 0;
  int *piAddressVarOne = NULL;
  int *piVarOneNbItemRow = NULL;
  int *piVarOneColPos = NULL;
  double *pdblVarOneReal = NULL;
  double *pdblVarOneImg = NULL;
  int iType1 = 0;
  int complex_F;

  // second parameter: matrix G
  int m2 = 0, n2 = 0;
  int *piAddressVarTwo = NULL;
  double *pdVarTwo = NULL;
  doublecomplex *pdcVarTwo = NULL;
  int iType2 = 0;
  int complex_G;
  
  // third parameter: integer p
  int m3 = 0, n3 = 0;
  int *piAddressVarThree = NULL;
  double *pdVarThree = NULL;
  int iType3 = 0;
  int iVarThree = 0;

  // fourth parameter: integer q
  int m4 = 0, n4 = 0;
  int *piAddressVarFour = NULL;
  double *pdVarFour = NULL;
  int iType4 = 0;
  int iVarFour = 0;

  // fifth parameter (optional): integer k
  int m5 = 0, n5 = 0;
  int *piAddressVarFive = NULL;
  double *pdVarFive = NULL;
  int iType5 = 0;
  int iVarFive = 0;

  // first output: matrix L
  double *cOut1Real = NULL;
  double *cOut1Img = NULL;

  // second output: vector d
  double *dOut2 = NULL;

  // buffer for a return code
  int ret;
  
  // check that we have 4-5 input parameters
  CheckRhs(4,5);

  // check that we have 1-2 output parameters
  CheckLhs(1,2);
  
  // get address of the first input  
  sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
  if(sciErr.iErr)
  {
    printError(&sciErr, 0);
    return -1;
  }

  // get address of the second input
  sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
  if(sciErr.iErr)
  {
    printError(&sciErr, 0);
    return -2;
  }
  
  // get address of the third input
  sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddressVarThree);
  if(sciErr.iErr)
  {
    printError(&sciErr, 0);
    return -3;
  }

  // get address of the fourth input
  sciErr = getVarAddressFromPosition(pvApiCtx, 4, &piAddressVarFour);
  if(sciErr.iErr)
  {
    printError(&sciErr, 0);
    return -4;
  }

  // get address of the optinal fifth input
  if (Rhs>4)
  {
    sciErr = getVarAddressFromPosition(pvApiCtx, 5, &piAddressVarFive);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      return -5;
    }
  }

  // check input type of the first input
  sciErr = getVarType(pvApiCtx, piAddressVarOne, &iType1);
  if(sciErr.iErr)
  {
    printError(&sciErr, 0);
    return -1;
  }
  if (iType1!=sci_sparse)
  {
    Scierror(999, \
      "%s: Wrong type for input argument #%d: Sparse matrix expected.\n", \
      fname,1);
    return -1;
  }

  // check input type of the second input
  sciErr = getVarType(pvApiCtx, piAddressVarTwo, &iType2);
  if(sciErr.iErr)
  {
    printError(&sciErr, 0);
    return -2;
  }
  if (iType2!=sci_matrix)
  {
    Scierror(999, "%s: Wrong type for input argument #%d: Matrix expected.\n", \
      fname,2);
    return -2;
  }

  // check input type of the third input
  sciErr = getVarType(pvApiCtx, piAddressVarThree, &iType3);
  if(sciErr.iErr)
  {
    printError(&sciErr, 0);
    return -3;
  }
  if (iType3!=sci_matrix)
  {
    Scierror(999,"%s: Wrong type for input argument #%d: Scalar expected.\n", \
      fname,3);
    return -3;
  }

  // check input type of the fourth input
  sciErr = getVarType(pvApiCtx, piAddressVarFour, &iType4);
  if(sciErr.iErr)
  {
    printError(&sciErr, 0);
    return -4;
  }
  if (iType4!=sci_matrix)
  {
    Scierror(999,"%s: Wrong type for input argument #%d: Scalar expected.\n", \
      fname,4);
    return -4;
  }

  // check input type of the optional fifth input
  if (Rhs>4)
  {
    sciErr = getVarType(pvApiCtx, piAddressVarFive, &iType5);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      return -5;
    }
    if (iType5!=sci_matrix)
    {
      Scierror(999, \
	"%s: Wrong type for input argument #%d: Scalar expected.\n", \
	fname,5);
      return -5;
    }
  }

  // check which parameters are complex
  complex_F = isVarComplex(pvApiCtx, piAddressVarOne); 
  complex_G = isVarComplex(pvApiCtx, piAddressVarTwo);

  // get parameter one  
  if (complex_F)
  {
    sciErr = getComplexSparseMatrix(pvApiCtx,piAddressVarOne,&m1,&n1,&items1, \
      &piVarOneNbItemRow,&piVarOneColPos,&pdblVarOneReal,&pdblVarOneImg);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      return -1;
    }
  }
  else
  {
    sciErr = getSparseMatrix(pvApiCtx,piAddressVarOne,&m1,&n1,&items1, \
      &piVarOneNbItemRow,&piVarOneColPos,&pdblVarOneReal);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      return -1;
    }
    if (complex_G)
    {
      pdblVarOneImg = malloc(sizeof(double)*items1);
      if (pdblVarOneImg==NULL)
      {
	Scierror(999,"%s: Insufficient memory.\n",fname);
	return 1;
      }
      memset(pdblVarOneImg,0,sizeof(double)*items1);
    }
  }

  // get parameter two
  if (complex_F||complex_G)
  {
    sciErr = getComplexZMatrixOfDouble(pvApiCtx,piAddressVarTwo,&m2,&n2, \
      &pdcVarTwo);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      if (!complex_F)
      {
	free(pdblVarOneImg);
      }
      return -2;
    }
  }
  else
  {
    sciErr = getMatrixOfDouble(pvApiCtx,piAddressVarTwo,&m2,&n2,&pdVarTwo);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      return -2;
    }
  }

  // get parameter three
  sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarThree,&m3,&n3,&pdVarThree);
  if(sciErr.iErr)
  {
    printError(&sciErr, 0);
    if ((!complex_F)&&(complex_G))
    {
      free(pdblVarOneImg);
    }
    return -3;
  }

  // get parameter four
  sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarFour,&m4,&n4,&pdVarFour);
  if(sciErr.iErr)
  {
    printError(&sciErr, 0);
    if ((!complex_F)&&(complex_G))
    {
      free(pdblVarOneImg);
    }
    return -4;
  }

  // get optional parameter five
  if (Rhs>4)
  {
    sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarFive,&m5,&n5,&pdVarFive);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      if ((!complex_F)&&(complex_G))
      {
	free(pdblVarOneImg);
      }
      return -5;
    }
  }

  // check size of parameter 1
  if ((m1==0) || (n1==0))
  {
    Scierror(999,"%s: Wrong size for input argument #%d: Argument is [].\n", \
      fname,1);
    if ((!complex_F)&&(complex_G))
    {
      free(pdblVarOneImg);
    }
    return -1;
  }

  // check that parameter 1 is square
  if (m1!=n1)
  {
    Scierror(999, \
      "%s: Wrong size for input argument #%d: Argument is not square.\n", \
      fname,1);
    if ((!complex_F)&&(complex_G))
    {
      free(pdblVarOneImg);
    }
    return -1;
  }

  // check size of parameter 2
  if ((m2==0) || (n2==0))
  {
    Scierror(999,"%s: Wrong size for input argument #%d: Argument is [].\n", \
      fname,2);
    if ((!complex_F)&&(complex_G))
    {
      free(pdblVarOneImg);
    }
    return -2;
  }

  // check that the sizes of the parameters 1 and 2 are compatible
  if (n1!=m2)
  {
    Scierror(999, \
      "%s: Sizes on the input argument #%d and #%d are incompatible.\n",fname, \
      1,2);
    if ((!complex_F)&&(complex_G))
    {
      free(pdblVarOneImg);
    }
    return -2;
  }

  // check size of parameter 3
  if ((m3!=1) || (n3!=1))
  {
    Scierror(999, \
      "%s: Wrong size for input argument #%d: A scalar expected.\n",fname,3);
    if ((!complex_F)&&(complex_G))
    {
      free(pdblVarOneImg);
    }
    return -3;
  }

  // check size of parameter 4
  if ((m4!=1) || (n4!=1))
  {
    Scierror(999, \
      "%s: Wrong size for input argument #%d: A scalar expected.\n",fname,4);
    if ((!complex_F)&&(complex_G))
    {
      free(pdblVarOneImg);
    }
    return -4;
  }

  // check size of optional parameter 5
  if (Rhs>4)
  {
    if ((m5!=1) || (n5!=1))
    {
      Scierror(999, \
	"%s: Wrong size for input argument #%d: A scalar expected.\n",fname,5);
      if ((!complex_F)&&(complex_G))
      {
	free(pdblVarOneImg);
      }
      return -5;
    }
  }

  // convert parameter 3 to integer and check value
  iVarThree = (int)pdVarThree[0];
  if (iVarThree<0)
  {
    Scierror(999, \
      "%s: Wrong range for input argument #%d: Should be at least zero.\n", \
      fname,3);
    if ((!complex_F)&&(complex_G))
    {
      free(pdblVarOneImg);
    }
    return -3;
  }

  // convert parameter 4 to integer and check value
  iVarFour = (int)pdVarFour[0];
  if (iVarFour<0)
  {
    Scierror(999, \
      "%s: Wrong range for input argument #%d: Should be at least zero.\n", \
      fname,4);
    if ((!complex_F)&&(complex_G))
    {
      free(pdblVarOneImg);
    }
    return -4;
  }

  // check that the sum of the parameters 3 and 4 is n2
  if (iVarThree+iVarFour!=n2)
  {
    Scierror(999,"%s: The width of G and p+q do not match.\n",fname);
    if ((!complex_F)&&(complex_G))
    {
      free(pdblVarOneImg);
    }
    return -4;
  }

  // convert optional parameter 5 to integer and check value
  if (Rhs>4)
  {
    iVarFive = (int)pdVarFive[0];
    if ((iVarFive<1)||(iVarFive>m1))
    {
      Scierror(999, \
	"%s: Wrong range for input argument #%d: Should be in the range [%d,%d].\n", \
	fname,1,m1,5);
      if ((!complex_F)&&(complex_G))
      {
	free(pdblVarOneImg);
      }
      return -5;
    }
  }
  else	// set default value if parameter five hasn't been specified
  {
    iVarFive = m1;
  }

  // create output variables
  if (complex_F||complex_G)
  {
    sciErr = allocComplexMatrixOfDouble(pvApiCtx,Rhs+1,m1,iVarFive,&cOut1Real, \
      &cOut1Img);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      if ((!complex_F)&&(complex_G))
      {
	free(pdblVarOneImg);
      }
      return 1;
    }
  }
  else
  {
    sciErr = allocMatrixOfDouble(pvApiCtx,Rhs+1,m1,iVarFive,&cOut1Real);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      if ((!complex_F)&&(complex_G))
      {
	free(pdblVarOneImg);
      }
      return 1;
    }
  }
  LhsVar(1) = Rhs+1;
  sciErr = allocMatrixOfDouble(pvApiCtx,Rhs+2,iVarFive,1,&dOut2);
  if(sciErr.iErr)
  {
    printError(&sciErr, 0);
    if ((!complex_F)&&(complex_G))
    {
      free(pdblVarOneImg);
    }
    return 1;
  }
  LhsVar(2) = Rhs+2;

  // run the algorithm
  if (complex_F||complex_G)
  {
    ret = complex_ldl_gs(piVarOneNbItemRow,piVarOneColPos,pdblVarOneReal, \
      pdblVarOneImg,&pdcVarTwo[0],iVarThree,iVarFour,m1,iVarFive,cOut1Real, \
      cOut1Img,dOut2);
  }
  else
  {
    ret = real_ldl_gs(piVarOneNbItemRow,piVarOneColPos,pdblVarOneReal, \
      &pdVarTwo[0],iVarThree,iVarFour,m1,iVarFive,cOut1Real,dOut2);
  }

  // handle the results
  switch (ret)
  {
    case 0:	// no errors, return results to Scilab
      PutLhsVar();
      break;
    case 1:	// routine ran out of memory
      Scierror(999,"%s: Insufficient memory.\n",fname);
      break;
    case 2:	// routine ran into numerical problems
      Scierror(999,"%s: Matrix has a zero leading minor.\n",fname);
      break; 
  }

  // free memory of internal variables
  if ((!complex_F)&&(complex_G))
  {
    free(pdblVarOneImg);
  }

  return ret;
}
