// ====================================================================
// Allan CORNET
// Simon LIPP
// INRIA 2008
// This file is released into the public domain
// ====================================================================

if getos() == "Windows" then
  // to manage long pathname
  includes_src_c = '-I""' + get_absolute_file_path('builder_gateway_c.sce') + '../../src/c""';
else
  includes_src_c = '-I' + get_absolute_file_path('builder_gateway_c.sce') + '../../src/c';
end

// PutLhsVar managed by user
WITHOUT_AUTO_PUTLHSVAR = %t;

tbx_build_gateway('fsmtbx_c', ['ldl_gs','sci_ldl_gs'], ['sci_ldl_gs.c'], ...
                  get_absolute_file_path('builder_gateway_c.sce'), ...
                  ['../../src/c/libldl_gs'],'',includes_src_c);

clear WITHOUT_AUTO_PUTLHSVAR;

clear tbx_build_gateway;
